#!/bin/bash

# Выводим информацию о программе
echo "Автор: Тухватулина Карина Амировна"
echo "Название программы: Удаление файлов больше заданного размера"
echo "Описание: Скрипт запрашивает путь к каталогу и размер файла, находит все файлы больше заданного размера и удаляет их, предварительно спрашивая подтверждения пользователя. Используется программа find."

mkdir root/test
echo "Nihdvhi HIGic kdvbiubv dhdhjshvd dshbshfvb fshvbfj gjvjv jggvvg" > root/test/test1.txt
touch root/test/test2.txt

while true; do
    # Запрашиваем путь к каталогу
    read -p "Введите путь к каталогу: " directory

    # Проверяем, что каталог существует
    if [ ! -d $directory ]; then
        echo "Каталог не существует."
        continue
    fi

    # Запрашиваем размер файла
    read -p "Введите размер файла в байтах: " size

    # Проверяем, что введен корректный размер файла
    if ! [[ "$size" =~ ^[0-9]+$ ]]; then
        echo "Размер файла должен быть целым числом."
        continue
    fi

    echo "Файлы в каталоге:"
    ls $directory

    # Используем программу find для поиска файлов и удаления их
    find $directory -type f -size +${size}c -print -exec rm -i {} \;

    echo "Файлы, оставшиеся в каталоге:"
    ls $directory

    # Запрашиваем пользователя, хочет ли он продолжить работу со скриптом
    read -p "Хотите продолжить работу со скриптом? (y/n) " answer
    case $answer in
        [Nn]* )
            echo "Выход из программы."
            exit 0
            ;;
        * )
            continue
            ;;
    esac
done
